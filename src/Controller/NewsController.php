<?php

namespace App\Controller;

use App\Entity\News;
use App\Form\NewsType;
use App\Repository\NewsRepository;
use App\Repository\TagRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Asset\Package;
use Symfony\Component\Asset\VersionStrategy\EmptyVersionStrategy;
use Symfony\Component\Cache\Adapter\FilesystemAdapter;

#[Route('/blog')]
class NewsController extends AbstractController
{
    private function getMastodonItems()
    {
        $mastond_feed=null;

        // Irakurri konfigurazioa
        $mastodon_rss = $this->getParameter('mastodon_feed');
        $mastodon_items=[];

        // Cache objektu berri bat sortu feed-a gordetzeko
        $cache = new FilesystemAdapter();
        $cached = $cache->getItem('mastodon_feed');
        $cached->expiresAfter(3600); /* ordu bat */

        // Cache ez badugu...
        if(!$cached->isHit()) {
            if($mastodon_feed = simplexml_load_string(file_get_contents($mastodon_rss)))
            {
                foreach($mastodon_feed->channel->item as $item)
                {
                    $mastodon_items[]=(array)$item;
                }
                $cached->set($mastodon_items);
                $cache->save($cached);
            }
        }
        else
        {
            $mastodon_items = $cached->get();
        }
        return $mastodon_items;
    }

    #[Route('/', name: 'app_news_index', methods: ['GET'])]
    public function index(Request $request, NewsRepository $newsRepository): Response
    {
        // Get last 10 news
        $last_news = $newsRepository->findBy(
                                             array("public" => true),
                                             array("id" => "DESC"),
                                             10,
                                             0
                                         );
        $rss= $request->query->get('rss');
        if(!is_null($rss))
        {
            $response= $this->render('blog/index.rss.twig', [
                'extra_css' => ['blog.css'],
                'title' => 'Blog',
                'items'         => $last_news,
            ]);
            $response->headers->set('Content-Type','text/xml');
            return $response;
        }
        else
        {
            $mastond_feed=null;

            $mastodon_items = $this->getMastodonItems();

            return $this->render('blog/index.html.twig', [
                'extra_css' => ['blog.css'],
                'title' => 'Blog',
                'mastodon_items' => $mastodon_items,
                'items'         => $last_news,
            ]);
        }
    }

    #[Route('/', name: 'app_news_rss', methods: ['GET'])]
    public function rss(NewsRepository $newsRepository)
    {
        // Get last 10 news
        $last_news = $newsRepository->findBy(
                array(),
                array("id" => "DESC"),
                10,
                0
            );

        return $this->render('blog/index.html.twig', [
            'extra_css' => ['blog.css'],
            'items'         => $last_news,
        ]);
    }
    #[Route('/archives', name: 'app_news_archives', methods: ['GET'])]
    public function archives(NewsRepository $newsRepository): Response
    {
        // Get last 10 news
        $last_news = $newsRepository->findBy(array(), array("id" => "Desc"));

        return $this->render('blog/archives.html.twig', [
            'extra_css' => ['archives.css'],
            'section' => 'Archives',
            'title' => 'Archives',
            'items'         => $last_news,
        ]);
    }

    #[Route('/admin/new', name: 'app_news_new', methods: ['GET', 'POST'])]
    public function new(Request $request, EntityManagerInterface $entityManager): Response
    {
        $news = new News();
        $form = $this->createForm(NewsType::class, $news);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $file = $form->get('image')->getData();
            if($file)
            {
                $fileName = $file->getClientOriginalName().'-'.uniqid().'.'.$file->guessExtension();
                $file->move($this->getParameter('blog_path'), $fileName);
                $news->setImage($fileName);
            }
            $entityManager->persist($news);
            $entityManager->flush();

            return $this->redirectToRoute('app_news_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->render('news/new.html.twig', [
            'extra_css' => ['blog.css','admin.css'],
            'news' => $news,
            'form' => $form,
        ]);
    }

    #[Route('/{url}.html', name: 'app_news_show', requirements: ['url' => '[^\/]+'], methods: ['GET'])]
    public function show($url, NewsRepository $newsRepository): Response
    {
        // Get last 10 news
        $last_news = $newsRepository->findBy(array("url" => $url));
        if(!$last_news)
        {
            throw $this->createNotFoundException('The news does not exist.');
        }

        return $this->render('blog/item.html.twig', [
            'extra_css' => ['blog.css','prism.css'],
            'extra_js' => ['prism.js'],
            'title' => $last_news[0]->getNom(),
            'item'         => $last_news[0],
        ]);
    }

    #[Route('/tag/{tag}.html', requirements: ['tag' => '.*'], name: 'app_news_tag', methods: ['GET'])]
    public function tag_search($tag, TagRepository $tagRepository, NewsRepository $newsRepository)
    {
        // Get last 10 news
        $tag_found = $tagRepository
            ->findBy(array("name" => $tag));

        $last_news = $newsRepository
            ->createQueryBuilder('n')
            ->innerJoin('n.Tags', 't')
            ->where('t.name = :tag_name')
            ->setParameter('tag_name', $tag)
                ->getQuery()->getResult();
        if(!$last_news)
        {
            throw $this->createNotFoundException('No news with tag "."$tag." found.');
        }
        $mastodon_items = $this->getMastodonItems();

        return $this->render('blog/index.html.twig', [
            'extra_css' => ['blog.css'],
            'section'       => $tag,
            'mastodon_items' => $mastodon_items,
            'title' => $tag,
            'items'         => $last_news,
        ]);
    }

    #[Route('/admin/tinymce-upload/image', name: 'app_news_upload')]
    public function upload(Request $request): Response
    {
        // @TODO: Set your own domain(s) in `$allowedOrigins`
        $allowedOrigins = ["https://localhost", "https://tfe.eus","https://tfe.eus.localhost"];
        $origin = $request->server->get('HTTP_ORIGIN');

        // same-origin requests won't set an origin. If the origin is set, it must be valid.
        if ($origin && !in_array($origin, $allowedOrigins)) {
            return new Response("You do not have access to this resource.", 403);
        }

        // Don't attempt to process the upload on an OPTIONS request
        if ($request->isMethod("OPTIONS")) {
            return new Response("", 200, ["Access-Control-Allow-Methods" => "POST, OPTIONS"]);
        }

        /** @var UploadedFile|null */
        $file = $request->files->get("file");

        if (!$file) {
            return new Response("Missing file.", 400);
        }

        if (!str_starts_with($file->getMimeType(), "image/")) {
            return new Response("Provided file is not an image.", 400);
        }

        $fileUrl = "";

        $fileName = $file->getClientOriginalName().'-'.uniqid().'.'.$file->guessExtension();
        $file->move($this->getParameter('blog_path'), $fileName);
        $package = new Package(new EmptyVersionStrategy());

        return new JsonResponse(
            ["location" => $package->getUrl($this->getParameter('blog_directory').'/'.$fileName)],
            200,
            [
                "Access-Control-Allow-Origin" => $origin,
                "Access-Control-Allow-Credentials" => true,
                "P3P" => 'CP="There is no P3P policy."',
            ],
        );
    }

    #[Route('/admin/{id}/edit', name: 'app_news_edit', methods: ['GET', 'POST'])]
    public function edit(Request $request, News $news, EntityManagerInterface $entityManager): Response
    {
        $url = $this->generateUrl('app_news_upload')."?";
        $form = $this->createForm(NewsType::class, $news, [
            'upload_url' => $url
        ]);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $file = $form->get('image')->getData();
            if($file)
            {
                $fileName = $file->getClientOriginalName().'-'.uniqid().'.'.$file->guessExtension();
                $file->move($this->getParameter('blog_path'), $fileName);
                $news->setImage($fileName);
            }

            $entityManager->flush();

            return $this->redirectToRoute('app_news_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->render('news/edit.html.twig', [
            'extra_css' => ['blog.css','admin.css'],
            'news' => $news,
            'form' => $form,
        ]);
    }

    #[Route('/admin/{id}', name: 'app_news_delete', methods: ['POST'])]
    public function delete(Request $request, News $news, EntityManagerInterface $entityManager): Response
    {
        if ($this->isCsrfTokenValid('delete'.$news->getId(), $request->getPayload()->getString('_token'))) {
            $entityManager->remove($news);
            $entityManager->flush();
        }

        return $this->redirectToRoute('app_news_index', [], Response::HTTP_SEE_OTHER);
    }
}
