<?php

namespace App\Form;

use App\Entity\Categorie;
use App\Entity\News;
use App\Entity\Tag;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Eckinox\TinymceBundle\Form\Type\TinymceType;


class NewsType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('nom')
            ->add('image', FileType::class, [
                'mapped' => false,
                'required' => false
            ])
            ->add('contenu', TinymceType::class,  array(
                'attr' => array(
                    "height"=> 1000,
                    "plugins" => "advlist autolink link image media table lists codesample",
                    "images_upload_url" => $options['upload_url'],
                    "images_upload_route_params" => "images_upload_url",
                    "toolbar" => "bold italic underline | bullist numlist code codesample",
                ))
            )
            ->add('date', null, [
                'widget' => 'single_text',
            ])
            ->add('url')
            ->add('tags', EntityType::class, [
                'class' => Tag::class,
                'choice_label' => 'name',
                'expanded' => true,
                'multiple' => true,
            ])
            ->add('tiny')
            ->add('public')
            ->add('relation', EntityType::class, [
                'class' => Categorie::class,
                'choice_label' => 'id',
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'upload_url' => '',
            'data_class' => News::class,
        ]);
    }
}
